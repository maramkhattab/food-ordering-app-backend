package com.sumerge.foodorderingapp.Repositories;

import com.sumerge.foodorderingapp.Models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findByUserId(int userId);
}
