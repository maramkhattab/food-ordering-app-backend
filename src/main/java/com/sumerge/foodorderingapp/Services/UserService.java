package com.sumerge.foodorderingapp.Services;

import com.sumerge.foodorderingapp.Models.User;
import com.sumerge.foodorderingapp.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepo;

    public List<User> getAllUsers() {

        return userRepo.findAll();
    }

    public Optional<User> findById(int id) {

        return userRepo.findById(id);
    }

    public Optional<User> findByName(String name) {

        return userRepo.findByUserName(name);
    }

    public User save(User itm) {

        return userRepo.save(itm);
    }

    public void deleteById(int id) {

        userRepo.deleteById(id);
    }
}
