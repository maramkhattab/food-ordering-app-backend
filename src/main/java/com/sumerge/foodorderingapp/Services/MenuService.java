package com.sumerge.foodorderingapp.Services;

import com.sumerge.foodorderingapp.Models.MenuItem;
import com.sumerge.foodorderingapp.Repositories.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MenuService {
    @Autowired
    MenuRepository menuRepo;

    public List<MenuItem> getAllMenuItems() {

        return menuRepo.findAll();
    }

    public Optional<MenuItem> findById(int id) {

        return menuRepo.findById(id);
    }

    public Optional<MenuItem> findByName(String name) {

        return menuRepo.findByName(name);
    }

    public MenuItem save(MenuItem itm) {

        return menuRepo.save(itm);
    }

    public void deleteById(int id) {

       menuRepo.deleteById(id);
    }
}
