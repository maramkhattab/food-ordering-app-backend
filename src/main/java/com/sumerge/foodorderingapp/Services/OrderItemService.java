package com.sumerge.foodorderingapp.Services;

import com.sumerge.foodorderingapp.Models.Order;
import com.sumerge.foodorderingapp.Models.OrderItem;
import com.sumerge.foodorderingapp.Repositories.MenuRepository;
import com.sumerge.foodorderingapp.Repositories.OrderItemRepository;
import com.sumerge.foodorderingapp.Repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderItemService {
    @Autowired
    OrderItemRepository orderItemRepository;
    @Autowired
    OrderService orderService;
    @Autowired
    MenuService menuService;

    public List<OrderItem> getAllOrders() {

        return orderItemRepository.findAll();
    }

    public Optional<OrderItem> findById(int id) {

        return orderItemRepository.findById(id);
    }


    public OrderItem save(OrderItem orderItem) throws Exception {



                return orderItemRepository.save(orderItem);



    }


    public void deleteById(int id) {

        orderItemRepository.deleteById(id);
    }

//    public List<OrderItem> getItemsForOrder(int orderId) {
//        return  orderItemRepository.findByOrderId(orderId);
//    }

}
