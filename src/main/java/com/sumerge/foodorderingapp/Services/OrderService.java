package com.sumerge.foodorderingapp.Services;

import com.sumerge.foodorderingapp.Models.MenuItem;
import com.sumerge.foodorderingapp.Models.Order;
import com.sumerge.foodorderingapp.Models.OrderBody;
import com.sumerge.foodorderingapp.Models.OrderItem;
import com.sumerge.foodorderingapp.Repositories.OrderRepository;
import com.sumerge.foodorderingapp.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    MenuService menuService;
    @Autowired
    OrderItemService orderItemService;


    public List<Order> getAllOrders() {

        return orderRepository.findAll();
    }

    public Optional<Order> findById(int id) {

        return orderRepository.findById(id);
    }
    public Order addItemsToOrder(List<OrderBody> orderItems,Order order) throws Exception {
        orderRepository.save(order);
        int total=0;
        for (OrderBody itm: orderItems){
            MenuItem menuItem= menuService.findByName(itm.getMenuItemName()).orElseThrow(()->new NullPointerException());
            OrderItem orderItem=new OrderItem();
            orderItem.setOrder(order);
            orderItem.setMenuItem(menuItem);
            orderItem.setQuantity(itm.getQuantity());
            orderItemService.save(orderItem);
            total+=menuItem.getPrice()* itm.getQuantity();
        }
        order.setTotal(total);
        return orderRepository.save(order);

    }

    public Order save(Order order) throws Exception {

             return orderRepository.save(order);



    }

    public void deleteById(int id) {

        orderRepository.deleteById(id);
    }
    public List<Order> getOrdersForUser(int userId){
        return  orderRepository.findByUserId(userId);
    }


}
