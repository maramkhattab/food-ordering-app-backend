package com.sumerge.foodorderingapp.Interfaces;

import com.sumerge.foodorderingapp.Models.Order;
import com.sumerge.foodorderingapp.Models.OrderBody;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public interface OrderControllerInterface {
    @GetMapping(value="users/{id}/orders")
    public List<Order> getUserOrders(@PathVariable("id")  int id);
    @GetMapping(value="users/{id}/orders/{orderId}")
    public Optional<Order> getOrderById(@PathVariable("orderId")  int id);
    @PostMapping(value="users/{id}/orders",consumes = MediaType.APPLICATION_JSON_VALUE)
    public Order addOrder(@PathVariable("id")  int id, @RequestBody List<OrderBody> orderItems) throws Exception;
    @PutMapping(value="users/{id}/orders/{orderId}")
    public Order updateOrder( @PathVariable("id")  int userId,@PathVariable("orderId") int orderId,@RequestBody List<OrderBody> orderItems) throws Exception;
    @DeleteMapping (value="users/{id}/orders/{orderId}")
    public void removeOrder(@PathVariable("orderId") int orderId);

}
