package com.sumerge.foodorderingapp.Interfaces;

import com.sumerge.foodorderingapp.Models.MenuItem;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

public interface MenuControllerInterface {
    @GetMapping("/menu")
    public List<MenuItem> getAllItems();
    @GetMapping("/menu/{id}")
    public Optional<MenuItem> getMenuItemById(@PathVariable("id")  int id);
    @PostMapping(value="/menu")
    public HttpStatus addMenuItem(@RequestBody MenuItem itm) throws HttpClientErrorException;
    @PutMapping(value = "/menu/{id}")
    public HttpStatus updateMenuItem(@PathVariable("id")  int id, @RequestBody MenuItem itm);
    @DeleteMapping(value="menu/{id}")
    public  HttpStatus  deleteMenuItem(@PathVariable("id")  int id);
}
