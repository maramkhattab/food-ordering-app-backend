package com.sumerge.foodorderingapp.Interfaces;

import com.sumerge.foodorderingapp.Models.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public interface UserControllerInterface {
    @PostMapping(value="/signup")
    public User addUser(@RequestBody User user);
    @GetMapping(value="/users")
    public List<User> getUsers();
    @GetMapping(value="/users/{id}")
    public Optional<User> getUserById(@PathVariable("id")  int id);
    @PutMapping(value="/users/{id}")
    public User updateUser(@PathVariable("id")  int id,@RequestBody User user);
    @DeleteMapping (value="/users/{id}")
    public void deleteUser(@PathVariable("id")  int id);
}
