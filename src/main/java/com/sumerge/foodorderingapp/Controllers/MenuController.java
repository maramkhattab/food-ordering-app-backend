package com.sumerge.foodorderingapp.Controllers;

import com.sumerge.foodorderingapp.Interfaces.MenuControllerInterface;
import com.sumerge.foodorderingapp.Models.MenuItem;
import com.sumerge.foodorderingapp.Services.MenuService;
import com.sumerge.foodorderingapp.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@RestController
public class MenuController implements MenuControllerInterface {
    @Autowired
    MenuService menuService;

    @Autowired
    UserService userService;

    public boolean isAdmin(){
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_ADMIN"));

    }


    public List<MenuItem> getAllItems(){
        return menuService.getAllMenuItems();
    }

    public Optional<MenuItem> getMenuItemById(@PathVariable("id")  int id){
        return menuService.findById(id);
    }



    public HttpStatus addMenuItem(@RequestBody MenuItem itm) throws  HttpClientErrorException {
        boolean allowed=  this.isAdmin();
        if(allowed) {

            menuService.save(itm);
            return HttpStatus.ACCEPTED;
        }
        else{

            return  HttpStatus.UNAUTHORIZED;
        }
    }


    public HttpStatus updateMenuItem(@PathVariable("id")  int id, @RequestBody MenuItem itm){
        boolean allowed=  this.isAdmin();
        if(allowed) {

            itm.setItemId(id);
            menuService.save(itm);
            return HttpStatus.ACCEPTED;
        }
        else{

            return  HttpStatus.UNAUTHORIZED;
        }


    }

    public  HttpStatus  deleteMenuItem(@PathVariable("id")  int id){
        boolean allowed=  this.isAdmin();
        if(allowed) {
            menuService.deleteById(id);

            return HttpStatus.ACCEPTED;
        }
        else{

            return  HttpStatus.UNAUTHORIZED;
        }

    }

}
