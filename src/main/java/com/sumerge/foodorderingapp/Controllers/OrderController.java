package com.sumerge.foodorderingapp.Controllers;

import com.sumerge.foodorderingapp.Interfaces.OrderControllerInterface;
import com.sumerge.foodorderingapp.Models.*;
import com.sumerge.foodorderingapp.Services.MenuService;
import com.sumerge.foodorderingapp.Services.OrderItemService;
import com.sumerge.foodorderingapp.Services.OrderService;
import com.sumerge.foodorderingapp.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class OrderController  implements OrderControllerInterface {
    @Autowired
    OrderService orderService;
    @Autowired
    UserService userService;
    @Autowired
    OrderItemService orderItemService;
    @Autowired
    MenuService menuService;


    public List<Order> getUserOrders(@PathVariable("id")  int id){
        return orderService.getOrdersForUser(id);
    }

    public Optional<Order> getOrderById(@PathVariable("orderId")  int id){
        return orderService.findById(id);
    }

    public Order addOrder(@PathVariable("id")  int id, @RequestBody List<OrderBody> orderItems) throws Exception {

        Order order=new Order();
        User user = userService.findById(id).orElseThrow(()->new Exception("User with ID " + id + "not found"));
        order.setUser(user);

        return   orderService.addItemsToOrder(orderItems,order);
    }

    public Order updateOrder( @PathVariable("id")  int userId,@PathVariable("orderId") int orderId,@RequestBody List<OrderBody> orderItems) throws Exception {
        Order order=orderService.findById(orderId).orElseThrow(()->new NullPointerException());

        return orderService.addItemsToOrder(orderItems,order);
    }

    public void removeOrder(@PathVariable("orderId") int orderId){
        orderService.deleteById(orderId);
    }

}
