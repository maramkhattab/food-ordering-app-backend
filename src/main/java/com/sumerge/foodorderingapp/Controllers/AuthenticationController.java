package com.sumerge.foodorderingapp.Controllers;

import com.sumerge.foodorderingapp.Interfaces.AuthenticationControllerInterface;
import com.sumerge.foodorderingapp.Util.JwtUtil;
import com.sumerge.foodorderingapp.Models.AuthenticationRequest;
import com.sumerge.foodorderingapp.Models.AuthenticationResponse;
import com.sumerge.foodorderingapp.Services.AppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController implements AuthenticationControllerInterface {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    AppUserDetailsService userDetailsService;
    @Autowired
    JwtUtil jwtTokenUtil;


    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authRequest) throws Exception{
       try {
           authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(authRequest.getUsername(),authRequest.getPassword()));

       }
       catch (BadCredentialsException e){
           throw new Exception("Incorrect username or password",e);
       }
       final UserDetails userDetails=userDetailsService.loadUserByUsername(authRequest.getUsername());
       final String jwt= jwtTokenUtil.generateToken(userDetails);
       return ResponseEntity.ok(new AuthenticationResponse(jwt));
     }
}
