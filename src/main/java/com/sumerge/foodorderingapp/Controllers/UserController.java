package com.sumerge.foodorderingapp.Controllers;


import com.sumerge.foodorderingapp.Interfaces.UserControllerInterface;
import com.sumerge.foodorderingapp.Models.User;
import com.sumerge.foodorderingapp.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController implements UserControllerInterface {
    @Autowired
    UserService userService;

    public User addUser(@RequestBody User user) {
        user.setRoles("role_user");
        return userService.save(user);
    }

    public List<User> getUsers(){
        return userService.getAllUsers();
    }


    public Optional<User> getUserById(@PathVariable("id")  int id) {
        Optional<User> user = userService.findById(id);

        return user;
    }

    public User updateUser(@PathVariable("id")  int id,@RequestBody User user) {
        user.setId(id);

       return userService.save(user);


    }

    public void deleteUser(@PathVariable("id")  int id) {


        userService.deleteById(id);


    }


}
