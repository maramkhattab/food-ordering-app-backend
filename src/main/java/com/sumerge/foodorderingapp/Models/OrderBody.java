package com.sumerge.foodorderingapp.Models;

public class OrderBody {
    public String MenuItemName;
    public int quantity;

    public OrderBody(String menuItemName, int quantity) {
        MenuItemName = menuItemName;
        this.quantity = quantity;
    }

    public OrderBody() {
    }

    public String getMenuItemName() {
        return MenuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        MenuItemName = menuItemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
